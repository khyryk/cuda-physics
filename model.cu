#include <cuda.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>


static const int TIME_INTERVAL = 10; // seconds
static const float G_CONST = 6.67384e-20f; // N * km2 / kg2

typedef struct {
    // vector quantities {x, y, z}
    float gravitationalForce[3];
    float acceleration[3];
    float velocity[3];
    float position[3];
    
    // scalar quantities
    float mass;
} stellarObject;


//==========PHYSICS FUNCTIONS============================================================
static __device__ float getVectorLength(float PQ[]) {
	return sqrt( pow(PQ[0], 2) + pow(PQ[1], 2) + pow(PQ[2], 2) );
}

__device__ void calcGravitationalForce(float P[], float Q[], float m1, float m2, float Fg[]) {
	// for acts on object at position P in the direction PQ
	float PQ[] = {0, 0, 0};
	for (int i = 0; i < 3; i++) {
	    PQ[i] = Q[i] - P[i];
	}
	for (int i = 0; i < 3; i++) {
		Fg[i] += (G_CONST * m1 * m2) * (PQ[i] / getVectorLength(PQ)) / pow(getVectorLength(PQ), 2); // reset to 0 after every iteration
	}
}

static __device__ void calcAcceleration(float Fg[], float m, float acceleration[]) {
	for (int i = 0; i < 3; i++) {
		acceleration[i] = Fg[i] / m;
	}
}

static __device__ void calcVelocity(float acceleration[], float velocity[]) {
	for (int i = 0; i < 3; i++) {
		velocity[i] += acceleration[i] * TIME_INTERVAL;
	}
}

static __device__ void updatePosition(float velocity[], float position[]) {
	for (int i = 0; i < 3; i++) {
		position[i] += velocity[i] * TIME_INTERVAL;
	}
}

__device__ void updateStellarObject(stellarObject *s) {
	calcAcceleration(s->gravitationalForce, s->mass, s->acceleration);
	calcVelocity(s->acceleration, s->velocity);
	updatePosition(s->velocity, s->position);
}
//=======================================================================================
 // i = blockIdx.x * 512 + threadIdx.x
__global__ void runSimulation(unsigned long long time, int numElements, stellarObject objlistPtr[]) {
    int i = threadIdx.x;
    if (i >= numElements) return;
    for (int g = 0; g < 3; g++) {
        objlistPtr[i].gravitationalForce[g] = 0;
        objlistPtr[i].acceleration[g] = 0;
        //objlistPtr[i].velocity[g] = 0;
    }
    
    for (unsigned long long t = 0ULL; t < time; t++) { // calculate Fg
        for (int n = 0; n < numElements; n++) {
            if (i == n);
            else {
                calcGravitationalForce(objlistPtr[i].position, objlistPtr[n].position,
                                       objlistPtr[i].mass, objlistPtr[n].mass,
                                       objlistPtr[i].gravitationalForce);
            }
        }
            
        updateStellarObject(&objlistPtr[i]); // use Fg to calculate accel -> vel -> position
        for (int g = 0; g < 3; g++) {
            objlistPtr[i].gravitationalForce[g] = 0.0f; // reset Fg after each update
        }
        
    }
    
}


int main() {
    printf("G constant: %g\n", G_CONST);
    
    stellarObject Earth, Moon;
    Earth.position[0] = 0.0f; Earth.position[1] = 0.0f; Earth.position[2] = 0.0f;
    Moon.position[0] = 384400.0f; Moon.position[1] = 0.0f; Moon.position[2] = 0.0f;
    
    Earth.mass = 5.974e24f;
    Moon.mass = 7.348e22f;
    
    Earth.velocity[0] = 0.0f; Earth.velocity[1] = 0.0f; Earth.velocity[2] = 0.0f;
    Moon.velocity[0] = 0.0f; Moon.velocity[1] = 1.020f; Moon.velocity[2] = 0.0f;
    
    stellarObject objlist[] = {Earth, Moon};
    
    
    int numObjects = sizeof(objlist) / sizeof(objlist[0]);
    stellarObject *optr;
    
    cudaMalloc(&optr, numObjects * sizeof(stellarObject));
    cudaMemcpy(optr, objlist, numObjects * sizeof(stellarObject), cudaMemcpyHostToDevice); // copy array to video card
    
    // tasks running for more than 5 seconds require headless GPU
    runSimulation<<<1, 128>>>(14ULL * 24 * 60 * 60 / TIME_INTERVAL, numObjects, optr);
    
    cudaMemcpy(objlist, optr, numObjects * sizeof(stellarObject), cudaMemcpyDeviceToHost); // get array back
    
    for (int i = 0; i < numObjects; i++) {
        printf("%d: <%.0f, %.0f>\n", i, objlist[i].position[0] / 1000, objlist[i].position[1] / 1000);
    }
    
    cudaFree(optr);
    
    printf("Struct size: %lu\n\n", sizeof(stellarObject));
    return 0;
}

